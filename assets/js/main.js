$(document).ready(function() {
    function checkTime(i) {
        if(i < 10) {
            i = "0" + i;
        }
        return i;
    }
    function getWeekDate(i) {
        const WeekDates = ["Sunday","Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        let currentWeekDate = WeekDates[i];
        return currentWeekDate;
    }
    $("#get-date").click(function displayCurrentTime() {
            let date = new Date();
            let day = date.getDay();
            day = getWeekDate(day);
            let timeHour = date.getHours();
            let timeMinutes = date.getMinutes();
            timeMinutes = checkTime(timeMinutes);
            let timeSeconds = date.getSeconds();
            timeSeconds = checkTime(timeSeconds);
            let currentPeriod;
            if(timeHour >= 12) {
                currentPeriod = " PM";
                $("#time").text("Current time is : " + (timeHour-12) + currentPeriod + " : " + timeMinutes + " : " + timeSeconds);
            }
            else {
                currentPeriod = " AM";
                $("#time").text("Current time is : " + timeHour + currentPeriod + " : " + timeMinutes + " : " + timeSeconds);
            }
            $("#day").text("Today is " + day);
            setTimeout(displayCurrentTime,1000);
    });
    // Bài 1 - Jquery

    $("#get-result").click(function() {
        let firstnumber = parseInt ($("#first-number").val());
        let secondnumber = parseInt ($("#second-number").val());
        let result=[];
        $("#result").html("");
        $("#error-message-1").html("");
        if(!firstnumber || !secondnumber) {
            $("#error-message-1").text("Error: Yo nigga enter every shit in the Input field");
            $("#audio")[0].play();
        }

        else if(firstnumber > secondnumber) {
            $("#first-number").val("");
            $("#second-number").val("");
            $("#error-message-1").text("Error: Yo nigga re-enter in an ascending order");
            $("#audio")[0].play();
        }
        else if(firstnumber == 1 && secondnumber == 1) {
            $("#first-number").val("");
            $("#second-number").val("");
            $("#error-message-1").text("Error: Yo nigga re-enter , you can't enter 1 in both input");
            $("#audio")[0].play();
        }
                else {
                    $("#result").text("Result: ");
                    $("#error-message-1").text("");
                    for(let i = firstnumber ; i <= secondnumber ; i++ ) {
                        let count=0; 
                        if(i == 1) { 
                            i++;
                        }
                        for(let j=2 ; j<= Math.sqrt(i) ; j++ ) {
                            if(i%j == 0) {
                                count++;
                            }
                        }
                        if(count == 0) {
                            result.push(i);
                        }
                    }
                    $.each(result , function(index, value) {
                        $("#result").html($("#result").html() + " <span> " + value + " </span> ")
                    });
                }   
    });
    // Bài 2 - Jquery

    $("#get-date-first").click(function() {
        let date = new Date();
        let day = date.getDate();
        let month = date.getMonth()+1;
        let year = date.getFullYear();
        $("#date-first").html("Output: " + month + "-" + day + "-" + year );
    });
    
    $("#get-date-second").click(function() {
        let date = new Date();
        let day = date.getDate();
        let month = date.getMonth()+1;
        let year = date.getFullYear();
        $("#date-second").html("Output: " + month + "/" + day + "/" + year );
    });
    
    $("#get-date-third").click(function() {
        let date = new Date();
        let day = date.getDate();
        let month = date.getMonth()+1;
        let year = date.getFullYear();
        $("#date-third").html("Output: " + day + "-" + month + "-" + year );
    });
    
    $("#get-date-fourth").click(function() {
        let date = new Date();
        let day = date.getDate();
        let month = date.getMonth()+1;
        let year = date.getFullYear();
        $("#date-fourth").html("Output: " + day + "/" + month + "/" + year );
    });
    // Bài 3 - Jquery

    function checkascending(array,length) {    
        for(let i= 0 ; i<= (length-2) ; i++) {
            if(array[i] >= array[i+1]) {
                return false;
            }
        }
    }
    $("#get-result-2").click(function() {
        let number= $("#checking-number").val();
        let length = number.length;
        let check= 0;
        $("#result-2").html("");
        $("#error-message-2").html("");
        if(length == 0) {
            $("#error-message-2").html("Error : Yo nigga please enter a number ");
            $("#audio")[0].play();
        }
        else if(length < 2 && length >0) {
            $("#error-message-2").html("Error : Yo nigga please enter a number larger than 9 ");
            $("#audio")[0].play();
        }
        else {
            if(checkascending(number,length) == 0) {
                $("#result-2").html("Dãy vừa nhập không là một dãy có các phần từ tăng");
            }
            else $("#result-2").html("Dãy vừa nhập là một dãy có các phần từ tăng");
        };
    });

    // Bài 4 - Jquery
    $("#get-result-3").click(function() {
        let words = $("#checking-word").val();
        let wordlength = words.length ;
        let result = "";
        $("#result-3").html("");
        $("#error-message-3").html("");
        if(!words) {
            $("#error-message-3").text("Error: Yo nigga enter every shit in the Input field");
            $("#audio")[0].play();
        }
        else {
            for(let i=0 ; i< wordlength; i++) {
                if(words.charCodeAt(i) === 122) {
                    result += "a";
                }
                else if(words.charCodeAt(i) === 90) {
                    result += "A";
                }
                else if((96 < words.charCodeAt(i) && words.charCodeAt(i) <122) || (64 < words.charCodeAt(i) && words.charCodeAt(i) < 90)) {
                    result += String.fromCharCode(words.charCodeAt(i)+1); // fromCharCode : Covert Unicode into string , charCodeAt : Return Unicode from string
                }
                else {
                        $("#error-message-3").text("Error: Yo nigga your input got a number  !");
                        $("#audio")[0].play();
                }
            }
            $("#result-3").html("Result: " + result);
        }
    });
    // Bài 5 - Jquery

    $("#get-result-4").click(function() {
        let oddnumbers = $("#checking-odd").val();
        let numberlength = oddnumbers.length ;
        let middlepoint = "";
        $("#result-4").html("");
        $("#error-message-4").html("");
        if(!oddnumbers) {
            $("#error-message-4").text("Error: Yo nigga enter every shit in the Input field");
            $("#audio")[0].play();
        }
        else if(numberlength < 3 ) {
            $("#error-message-4").text("Error: Yo nigga enter a number larger than 99");
            $("#audio")[0].play();
        }
        else if((numberlength % 2 )==0) {
            $("#error-message-4").text("Error: Yo nigga enter a number that has an odd length");
            $("#audio")[0].play();
        }
        else { 
            middlepoint = parseInt(numberlength/2);
            $("#result-4").html("Result: " + oddnumbers[middlepoint-1] + oddnumbers[middlepoint]+oddnumbers[middlepoint+1]);
        }
    });
    // Bài 6 - Jquery

    $("#get-result-5").click(function(){
        let scanword = $("#checking-p").val();
        let scanwordlength = scanword.length;
        let result ="";
        $("#result-5").html("");
        $("#error-message-5").html("");
        if(!scanword) {
            $("#error-message-5").text("Error: Yo nigga enter every shit in the Input field");
            $("#audio")[0].play();
        }
        else {
            result = scanword.replace(/[pP]/g,"");
            $("#result-5").html(result);
        }
    });
    // Bài 7 - Jquery
    function check(array1,array2) {
        let count=0;
        for(let i=0; i< array1.length; i++) {
            if(array1[i] != array2[i]) {
                count++;
            }
        }
        if(count >2) {
            return false;
        }
        else return true;
    }
    $("#get-result-6").click(function(){
        let firstarray = [];
        let secondarray = [];
        let firstelement = $("#first-array").val();
        let secondelement= $("#second-array").val();
        $("#result-6").html("");
        $("#error-message-6").html("");
        if(!firstelement || !secondelement) {
            $("#error-message-6").text("Error: Yo nigga enter some shit in the Input field");
            $("#audio")[0].play();
        }
        else {
            firstarray = firstelement.split(" ");
            secondarray = secondelement.split(" ");
            if(check(firstarray,secondarray)) {
                $("#result-6").html("2 Mảng vừa nhập tương tự nhau");
            }
            else {
                $("#result-6").html("2 Mảng vừa nhập không tương tự nhau");
            }
        }
    });
    // Bài 8 - Jquery
    function checkcount(array){
        let maxcount=0;
        let count=0;
        let maxnumber=0;
        array.sort();
        for(let i=0 ; i< array.length ; i++) {
            if(array[i] == array[i+1]) {
                count++;
                if(count > maxcount) {
                    maxcount = count;
                    maxnumber= array[i];
                }
            }
            else {
                count=0;
            }
        }
        if(maxcount) {
            $("#result-7").html("Result : Số lặp lại nhiều lần nhất là số : " + maxnumber + " với tất cả "+ (maxcount+1) + " lần lặp ");
        }
        else {
            $("#result-7").html("Result : Tất cả các số đều không có số nào lặp lại");
        }
    }
    $("#get-result-7").click(function(){
        let countarray = [];
        let countelement = $("#count-array").val();
        $("#result-7").html("");
        $("#error-message-7").html("");
        if(!countelement) {
            $("#error-message-7").text("Error: Yo nigga enter some shit in the Input field");
            $("#audio")[0].play();
        }
        else {
            countarray = countelement.split(" ");
            checkcount(countarray);
        }
    });
    // Bài 9 - Jquery

    $("#get-result-8").click(function() {
        let latin = $("#latin-array").val();
        $("#result-8").html("");
        $("#error-message-8").html("");
        if(!latin) {
            $("#error-message-8").text("Error: Yo nigga enter some shit in the Input field");
            $("#audio")[0].play();
        }
        else {
            let count =0 ;
            for(let i=0 ; i< latin.length -1 ; i++) {
                if( ( (64 < latin.charCodeAt(i) && latin.charCodeAt(i) <90) && 
                    (96 < latin.charCodeAt(i+1) && latin.charCodeAt(i+1) < 122)  ) || 
                ( (96 < latin.charCodeAt(i) && latin.charCodeAt(i) <122) && 
                (64 < latin.charCodeAt(i+1) && latin.charCodeAt(i+1) < 90) ) )  {
                    console.log("TRUE");
                }
                else {
                    count++;
                }
            }
            if(count) {
                $("#result-8").html("Không Thỏa mãn điều kiện đề");
                $("#audio")[0].play();
            }
            else {
                $("#result-8").html("Thỏa mãn điều kiện đề");
            }

        }

    });
    // Bài 10 - Jquery

    $("#get-result-11").click(function() {
        let java = $("#java-array").val();
        $("#result-11").html("");
        $("#error-message-11").html("");
        if(!java) {
            $("#error-message-11").text("Error: Yo nigga enter some shit in the Input field");
            $("#audio")[0].play();
        }
        else {
            let count = java.search("java");
            console.log(java);
            if (count>=0) {
                $("#result-11").html("Có chữ java trong chuỗi");
            }
            else {
                $("#result-11").html("không có chữ java trong chuỗi");
            }
        }
    });
    // Bài 13 - Jquery

    $("#get-result-12").click(function () { 
        let months = ["January" ,"February", "March" ,"April", "May" ,"June" ,"July", "August" ,"September" ,"October","November" ,"December"];
        let date = new Date($("#date-array").val());
        $("#result-12").html("");
        $("#error-message-12").html("");
        if(!$("#date-array").val()) {
            $("#error-message-12").text("Error: Yo nigga enter some shit in the Input field");
            $("#audio")[0].play();
        }
        else {
            let month = date.getMonth();
            $("#result-12").html("Tháng vừa nhập là : " + months[month]);
        }
    });
    // Bài 14 - Jquery
    $("#get-result-13").click(function () {  
        let input = $("#string-array").val();
        $("#result-13").html("");
        $("#error-message-13").html("");
        if(!input) {
            $("#error-message-13").text("Error: Yo nigga enter some shit in the Input field");
            $("#audio")[0].play();
        }
        else {
            let stringarray = input.split(" ");
            let maxlength = stringarray[0].length;
            let word =stringarray[0];
            for (let i=0 ; i< stringarray.length-1; i++) {
                if( (stringarray[i].length) < (stringarray[i+1].length) && ( stringarray[i+1].length > maxlength ) ) {
                    maxlength = stringarray[i+1].length;
                    word = stringarray[i+1];
                }
            }
            $("#result-13").html("Từ dài nhất là: " + word + " với độ dài " + maxlength);
        }
    // Bài 15 - Jquery
    });
});